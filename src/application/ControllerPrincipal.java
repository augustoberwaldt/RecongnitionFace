package application;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;
import org.opencv.objdetect.CascadeClassifier;

import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.StageStyle;

public class ControllerPrincipal implements Initializable {

	private byte[] foto;
	private Image image;
	
	@FXML
	private ImageView myImageView;
	
	@FXML
	private ImageView myImageView2;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) 
	{
		
		
	}
	
	@FXML
	private void abrirArquivo()
	{ 
	    
		FileChooser fileChooser = new FileChooser();
	
		File file = fileChooser.showOpenDialog(Main.stage);
		if (file != null) {
			BufferedImage bufferedImage;
			BufferedImage bufferedImage2;
			try {
	
			
				String tmpc = this.rec(file.getPath());
				File file2  = new File(tmpc);
				bufferedImage2 =  ImageIO.read(file2);
				bufferedImage  =  ImageIO.read(file);
				
				image = SwingFXUtils.toFXImage(bufferedImage, null);
				Image image2 = SwingFXUtils.toFXImage(bufferedImage2, null);
				
				myImageView.setFitWidth(500); 
				
				myImageView.setImage(image);
				myImageView.setX(21);
				myImageView.setY(10);
				
				
				myImageView2.setImage(image2);
				myImageView2.setX(21);
				myImageView2.setY(50);
				 	
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
			
	    	
		
	}
	
	private  String  rec (String path) 
	{
		    int x = 0,
	            y = 0,
	        height = 0,
	        width = 0;

		    System.load(new File(System.getProperty("user.dir") + "//src//lib//java//x64//opencv_java2413.dll").getAbsolutePath());
	    	
		    CascadeClassifier	 faceDetector = new CascadeClassifier();
		    faceDetector.load(System.getProperty("user.dir") + "//src//lib//haarcascades//haarcascade_frontalface_alt.xml");
		    
		    Mat image = Highgui.imread(path);
		    
		    MatOfRect faceDetections = new MatOfRect();
		    faceDetector.detectMultiScale(image, faceDetections);
		    
		    System.out.println(String.format("Detected %s faces", faceDetections.toArray().length));
		    
		    Rect rectCrop =null;
		    
		    for (Rect rect : faceDetections.toArray())
		    {
		        Core.rectangle(image, 
		        		new Point(rect.x, rect.y), 
		        		new Point(rect.x + rect.width, rect.y + rect.height),
		                new Scalar(20, 255, 12)
		        );
		        rectCrop = new Rect(rect.x, rect.y, rect.width, rect.height);
		        
		    }
		    try {
		        Mat image_roi = new Mat(image,rectCrop);
		    } catch (Exception e) {

			    JOptionPane.showMessageDialog(null, e.toString());
		    }
		    String tmpFile = System.getProperty ("java.io.tmpdir") + "teste.jpg";
		    
		    Highgui.imwrite(tmpFile, image);
	
		   return  tmpFile; 
	}
	

		
}
